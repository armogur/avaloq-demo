## Avaloq Demo

### Prerequisites
- JDK 17
- Gradle 7.6
- PostgreSQL 13.4-alpine

### Run
Run the spring boot application with this command:  
`./gradlew bootRun`  
This will bind to port 8080

### Generate OpenAPI contract

```shell
./gradlew generateOpenApiDocs
```

## Native compile
`./gradlew nativeCompile`  
Run native image: 
`./build/native/nativeCompile/demo`

### Usage
There are two links with GET request, so you can easily test it with a browser:
- http://localhost:8080/dice?dices=2&sides=4&rolls=17  
  The query parameters are optional, in this example we're rolling
  two 4 sided dices 17 times.
- http://localhost:8080/dice/statistics  
  In this link you can query the simulation statistics

### Design
I used **lombok** to eliminate boilerplate code writing.  
Database can be started up with docker-compose:
```shell
docker-compose -f docker-compose.yml up -d 
```
Made the configuration values (the dices, sides and rolls in query parameter) configurable through  
`ApplicationConfiguration.java`. There is an example `application-sandbox.yml` which overwrites the default requirements.  
Implemented my own validator in `DiceValidator.java` instead of using Java's own annotation based validation.