package ch.avaloq.demo;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author : istvan.horvath
 */
@ExtendWith(SpringExtension.class)
@PropertySource("file:src/main/resources/application.yml")
public class ApplicationConfigurationTest {

    @Test
    void constructorWithDefaults() {
        ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(null, null, null, null, null, null);

        assertThat(applicationConfiguration.getMinDices()).isEqualTo(1);
        assertThat(applicationConfiguration.getMinSides()).isEqualTo(4);
        assertThat(applicationConfiguration.getMinRolls()).isEqualTo(1);
        assertThat(applicationConfiguration.getDefaultDices()).isEqualTo(3);
        assertThat(applicationConfiguration.getDefaultSides()).isEqualTo(6);
        assertThat(applicationConfiguration.getDefaultRolls()).isEqualTo(100);
    }

    @Nested
    @TestPropertySource(properties = "spring.profiles.active=default")
    @EnableConfigurationProperties(value = ApplicationConfiguration.class)
    @ContextConfiguration(initializers = ConfigDataApplicationContextInitializer.class)
    class Default {

        @Autowired
        private ApplicationConfiguration applicationConfiguration;

        @Test
        void test() {
            assertThat(applicationConfiguration.getMinDices()).isEqualTo(1);
            assertThat(applicationConfiguration.getMinSides()).isEqualTo(4);
            assertThat(applicationConfiguration.getMinRolls()).isEqualTo(1);

            assertThat(applicationConfiguration.getDefaultDices()).isEqualTo(3);
            assertThat(applicationConfiguration.getDefaultSides()).isEqualTo(6);
            assertThat(applicationConfiguration.getDefaultRolls()).isEqualTo(100);
        }
    }

    @Nested
    @TestPropertySource(properties = "spring.profiles.active=sandbox")
    @EnableConfigurationProperties(value = ApplicationConfiguration.class)
    @ContextConfiguration(initializers = ConfigDataApplicationContextInitializer.class)
    class Sandbox {

        @Autowired
        private ApplicationConfiguration applicationConfiguration;

        @Test
        void test() {
            assertThat(applicationConfiguration.getMinDices()).isEqualTo(7);
            assertThat(applicationConfiguration.getMinSides()).isEqualTo(77);
            assertThat(applicationConfiguration.getMinRolls()).isEqualTo(777);

            assertThat(applicationConfiguration.getDefaultDices()).isEqualTo(8);
            assertThat(applicationConfiguration.getDefaultSides()).isEqualTo(88);
            assertThat(applicationConfiguration.getDefaultRolls()).isEqualTo(888);
        }
    }
}
