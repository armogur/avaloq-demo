package ch.avaloq.demo.services;

import org.junit.jupiter.api.RepeatedTest;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class DiceRollsTest {

    @RepeatedTest(49)
    void simulateRolls() {
        // GIVEN
        final int r = 3;
        final int d = 11;
        DiceRolls diceRolls = new DiceRolls(d, 7, r);

        // WHEN
        Rolls rolls = diceRolls.simulateRolls();

        // THEN
        assertThat(rolls.getRolls()).isEqualTo(r);

        Map<Long, Long> sumToRollTimes = rolls.getSumToRollTimes();

        assertThat(sumToRollTimes.keySet()).allMatch(i -> i >= d);
        assertThat(sumToRollTimes.values()).allMatch(i -> i >= 1);
        assertThat(sumToRollTimes.values().stream().mapToInt(Math::toIntExact).sum()).isEqualTo(r);
    }
}
