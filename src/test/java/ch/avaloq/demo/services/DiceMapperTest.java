package ch.avaloq.demo.services;

import ch.avaloq.demo.ApplicationConfiguration;
import ch.avaloq.demo.controllers.dto.*;
import ch.avaloq.demo.domain.SimulationStatistics;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DiceMapperTest {

    static final BigDecimal HUNDRED = BigDecimal.valueOf(100);
    ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(7, 77, 777,
            8, 88, 888);
    DiceMapper diceMapper = new DiceMapper(applicationConfiguration);

    @Nested
    class MapToDiceRolls {

        @Test
        void updateDefaultsOfUnset() {
            // GIVEN
            DiceRollsDto diceRollsDto = new DiceRollsDto(null, null, null);

            // WHEN
            DiceRolls diceRolls = diceMapper.mapToDiceRolls(diceRollsDto);

            // THEN
            assertThat(diceRolls.getDices()).isEqualTo(8);
            assertThat(diceRolls.getSides()).isEqualTo(88);
            assertThat(diceRolls.getRolls()).isEqualTo(888);
        }

        @Test
        void noDefaultSet() {
            // GIVEN
            DiceRollsDto diceRollsDto = new DiceRollsDto(1, 2, 3);

            // WHEN
            DiceRolls diceRolls = diceMapper.mapToDiceRolls(diceRollsDto);

            // THEN
            assertThat(diceRolls.getDices()).isEqualTo(1);
            assertThat(diceRolls.getSides()).isEqualTo(2);
            assertThat(diceRolls.getRolls()).isEqualTo(3);
        }
    }

    @Nested
    class MapToDiceDistributionSimulationDto {
        SimulationStatistics.SimulationId id = new SimulationStatistics.SimulationId(1, 3);

        SimulationStatistics simulationStatistics = mock(SimulationStatistics.class);

        @BeforeEach
        void setUp() {
            when(simulationStatistics.getId()).thenReturn(id);
            when(simulationStatistics.getTotalSimulations()).thenReturn(5L);
            when(simulationStatistics.getTotalRolls()).thenReturn(7L);
        }

        @Test
        void emptyRollTimes() {
            // GIVEN by setup and
            when(simulationStatistics.getSumToRollTimes()).thenReturn(Map.of());

            // WHEN
            DiceDistributionSimulationDto dto = diceMapper.mapToDiceDistributionSimulationDto(simulationStatistics);

            // THEN
            assertThat(dto.getDiceNumber()).isEqualTo(1);
            assertThat(dto.getDiceSide()).isEqualTo(3);
            assertThat(dto.getTotalSimulations()).isEqualTo(5);
            assertThat(dto.getTotalRolls()).isEqualTo(7);
            assertThat(dto.getSumToRoll()).isEmpty();
        }

        @Test
        void rollTimesNotEmpty() {
            // GIVEN
            when(simulationStatistics.getSumToRollTimes()).thenReturn(Map.of(13L, 17L));

            // WHEN
            DiceDistributionSimulationDto dto = diceMapper.mapToDiceDistributionSimulationDto(simulationStatistics);

            // THEN
            assertThat(dto.getDiceNumber()).isEqualTo(1);
            assertThat(dto.getDiceSide()).isEqualTo(3);
            assertThat(dto.getTotalSimulations()).isEqualTo(5);
            assertThat(dto.getTotalRolls()).isEqualTo(7);
            assertThat(dto.getSumToRoll()).isEqualTo(List.of(new DiceSumToRollDto(13L, 17L)));
        }
    }

    @Nested
    class MapToDiceDistributionStatistics {

        SimulationStatistics.SimulationId id = new SimulationStatistics.SimulationId(5, 7);

        SimulationStatistics simulationStatistics = mock(SimulationStatistics.class);

        @BeforeEach
        void setUp() {
            when(simulationStatistics.getId()).thenReturn(id);
            when(simulationStatistics.getTotalSimulations()).thenReturn(11L);
            when(simulationStatistics.getTotalRolls()).thenReturn(13L);
        }

        @Test
        void emptyRollTimes() {
            // GIVEN by setup and
            when(simulationStatistics.getSumToRollTimes()).thenReturn(Map.of());

            // WHEN
            DiceDistributionStatisticsDto statisticsDto = diceMapper.mapToDiceDistributionStatistics(
                    simulationStatistics);

            // THEN
            assertThat(statisticsDto.getDiceNumber()).isEqualTo(5);
            assertThat(statisticsDto.getDiceSide()).isEqualTo(7);
            assertThat(statisticsDto.getTotalSimulations()).isEqualTo(11);
            assertThat(statisticsDto.getTotalRolls()).isEqualTo(13);
            assertThat(statisticsDto.getDiceSumDistributions()).isEmpty();
        }

        @Test
        void rollTimesNotEmpty() {
            // GIVEN by setup and
            final Map<Long, Long> sumToRollTimes = Map.of(3L, 4L, 4L, 3L, 5L, 11L);
            when(simulationStatistics.getSumToRollTimes()).thenReturn(sumToRollTimes);

            // WHEN
            DiceDistributionStatisticsDto statisticsDto = diceMapper.mapToDiceDistributionStatistics(
                    simulationStatistics);

            // THEN
            assertStatisticsDto(statisticsDto);
        }

        @Test
        void emptyStatistics() {
            // WHEN
            List<DiceDistributionStatisticsDto> dtoList = diceMapper.mapToDiceDistributionStatistics(List.of());

            // THEN
            assertThat(dtoList).isEmpty();
        }

        @Test
        void statisticsNotEmpty() {
            // GIVEN by setup and
            final Map<Long, Long> sumToRollTimes = Map.of(3L, 4L, 4L, 3L, 5L, 11L);
            when(simulationStatistics.getSumToRollTimes()).thenReturn(sumToRollTimes);

            // WHEN
            List<DiceDistributionStatisticsDto> dtoList = diceMapper.mapToDiceDistributionStatistics(
                    List.of(simulationStatistics));

            assertThat(dtoList).hasSize(1);
            assertStatisticsDto(dtoList.get(0));
        }

        void assertStatisticsDto(DiceDistributionStatisticsDto statisticsDto) {
            assertThat(statisticsDto.getDiceNumber()).isEqualTo(5);
            assertThat(statisticsDto.getDiceSide()).isEqualTo(7);
            assertThat(statisticsDto.getTotalSimulations()).isEqualTo(11);
            assertThat(statisticsDto.getTotalRolls()).isEqualTo(13);
            assertThat(statisticsDto.getDiceSumDistributions()).containsExactlyInAnyOrder(
                    new DiceSumDistributionDto(3L, calculateDistribution(4, 13)),
                    new DiceSumDistributionDto(4L, calculateDistribution(3, 13)),
                    new DiceSumDistributionDto(5L, calculateDistribution(11, 13))
            );
        }
    }

    @Nested
    class MapDiceSumDistributions {

        @Test
        void emptyRollTimes() {
            // GIVEN
            final long totalRolls = 47;

            // WHEN
            List<DiceSumDistributionDto> dtoList = diceMapper.mapDiceSumDistributions(Map.of(), totalRolls);

            // THEN
            assertThat(dtoList).isEmpty();
        }

        @RepeatedTest(13)
        void rollTimesNotEmpty() {
            // GIVEN
            final Map<Long, Long> sumToRollTimes = Map.of(3L, 4L, 4L, 3L, 5L, 11L);
            final long totalRolls = 47;

            // WHEN
            List<DiceSumDistributionDto> dtoList = diceMapper.mapDiceSumDistributions(sumToRollTimes, totalRolls);

            // THEN
            assertThat(dtoList).containsExactly(
                    new DiceSumDistributionDto(3L, calculateDistribution(4, 47)),
                    new DiceSumDistributionDto(4L, calculateDistribution(3, 47)),
                    new DiceSumDistributionDto(5L, calculateDistribution(11, 47))
            );
        }
    }

    @Test
    void createSimulationId() {
        // GIVEN
        DiceRolls diceRolls = new DiceRolls(1, 2, 3);

        // WHEN
        SimulationStatistics.SimulationId simulationId = diceMapper.createSimulationId(diceRolls);

        // THEN
        assertThat(simulationId.getDiceNumber()).isEqualTo(1);
        assertThat(simulationId.getDiceSide()).isEqualTo(2);
    }

    @Test
    void createSimulationStatistics() {
        // GIVEN
        SimulationStatistics.SimulationId simulationId = mock(SimulationStatistics.SimulationId.class);

        // WHEN
        SimulationStatistics simulationStatistics = diceMapper.createSimulationStatistics(simulationId);

        // THEN
        assertThat(simulationStatistics.getId()).isSameAs(simulationId);
        assertThat(simulationStatistics.getTotalSimulations()).isEqualTo(0);
        assertThat(simulationStatistics.getTotalRolls()).isEqualTo(0);
    }

    BigDecimal calculateDistribution(long value, long totalRolls) {
        return HUNDRED.multiply(BigDecimal.valueOf(value))
                .divide(BigDecimal.valueOf(totalRolls), 2, RoundingMode.HALF_UP);
    }
}
