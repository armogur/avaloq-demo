package ch.avaloq.demo.services;

import ch.avaloq.demo.ApplicationConfiguration;
import ch.avaloq.demo.controllers.dto.DiceRollsDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * @author : istvan.horvath
 */
public class DiceValidatorTest {

    private DiceValidator diceValidator;

    @BeforeEach
    void setUp() {
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .minDices(1).minRolls(1).minSides(4).build();

        diceValidator = new DiceValidator(applicationConfiguration);
    }

    @Nested
    class ValidCases {

        @Test
        void allNull() {
            diceValidator.validate(new DiceRollsDto(null, null, null));
        }

        @Test
        void dicesOne() {
            diceValidator.validate(new DiceRollsDto(1, null, null));
        }

        @Test
        void sidesFour() {
            diceValidator.validate(new DiceRollsDto(null, 4, null));
        }

        @Test
        void rollsOne() {
            diceValidator.validate(new DiceRollsDto(null, null, 1));
        }

        @Test
        void allNonNull() {
            diceValidator.validate(new DiceRollsDto(1, 4, 1));
        }
    }

    @Nested
    class FailingCases {

        @Test
        void dicesZero() {
            assertThatThrownBy(() -> diceValidator.validate(new DiceRollsDto(0, null, null)))
                    .isInstanceOf(AvaloqValidationException.class).extracting("errors")
                    .asList().containsExactly("The number of dices should be at least 1, now: 0");
        }

        @Test
        void sidesThree() {
            assertThatThrownBy(() -> diceValidator.validate(new DiceRollsDto(1, 3, null)))
                    .isInstanceOf(AvaloqValidationException.class).extracting("errors")
                    .asList().containsExactly("The number of sides should be at least 4, now: 3");
        }

        @Test
        void rollsZero() {
            assertThatThrownBy(() -> diceValidator.validate(new DiceRollsDto(1, 4, 0)))
                    .isInstanceOf(AvaloqValidationException.class).extracting("errors")
                    .asList().containsExactly("The number of rolls should be at least 1, now: 0");
        }

        @Test
        void allNonNull() {
            assertThatThrownBy(() -> diceValidator.validate(new DiceRollsDto(0, 3, 0)))
                    .isInstanceOf(AvaloqValidationException.class).extracting("errors")
                    .asList().containsExactly("The number of dices should be at least 1, now: 0",
                    "The number of sides should be at least 4, now: 3",
                    "The number of rolls should be at least 1, now: 0");
        }
    }
}
