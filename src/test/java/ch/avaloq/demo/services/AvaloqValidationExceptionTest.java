package ch.avaloq.demo.services;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author : istvan.horvath
 */
public class AvaloqValidationExceptionTest {

    @Test
    void errorsIsNull() {
        AvaloqValidationException exception = new AvaloqValidationException(null);

        assertThat(exception.getErrors()).isNull();
    }

    @Test
    void errorsNotNull() {
        List<String> errors = new ArrayList<>();
        AvaloqValidationException exception = new AvaloqValidationException(errors);

        assertThat(exception.getErrors()).isSameAs(errors);
    }
}
