package ch.avaloq.demo.services;

import ch.avaloq.demo.controllers.dto.DiceDistributionSimulationDto;
import ch.avaloq.demo.controllers.dto.DiceDistributionStatisticsDto;
import ch.avaloq.demo.controllers.dto.DiceRollsDto;
import ch.avaloq.demo.domain.SimulationStatistics;
import ch.avaloq.demo.domain.SimulationStatisticsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author : istvan.horvath
 */
public class RollServiceTest {

    SimulationStatisticsRepository repository;
    DiceMapper diceMapper;
    RollService rollService;

    @BeforeEach
    void init() {
        repository = mock(SimulationStatisticsRepository.class);
        diceMapper = mock(DiceMapper.class);
        rollService = new RollService(repository, diceMapper);
    }

    @Nested
    class RollsRoyce {
        DiceRollsDto diceRollsDto;
        SimulationStatistics simulationStatistics;
        Rolls rolls;
        SimulationStatistics.SimulationId id;
        DiceDistributionSimulationDto resultDto;
        DiceRolls diceRolls;
        InOrder inOrder;

        @BeforeEach
        void setUp() {
            simulationStatistics = mock(SimulationStatistics.class);
            diceRollsDto = mock(DiceRollsDto.class);
            diceRolls = mock(DiceRolls.class);
            when(diceRolls.getDices()).thenReturn(3);
            when(diceRolls.getSides()).thenReturn(7);
            rolls = mock(Rolls.class);
            id = mock(SimulationStatistics.SimulationId.class);
            when(diceMapper.createSimulationId(diceRolls)).thenReturn(id);
            when(diceRolls.simulateRolls()).thenReturn(rolls);
            when(diceMapper.mapToDiceRolls(diceRollsDto)).thenReturn(diceRolls);
            resultDto = mock(DiceDistributionSimulationDto.class);
            when(diceMapper.mapToDiceDistributionSimulationDto(simulationStatistics)).thenReturn(resultDto);
            inOrder = Mockito.inOrder(diceRollsDto, diceRolls, diceMapper, repository, simulationStatistics);
        }

        @Test
        void statisticsFound() {
            // GIVEN by setup and
            when(repository.findById(id)).thenReturn(Optional.of(simulationStatistics));

            // WHEN
            DiceDistributionSimulationDto diceDistributionSimulationDto = rollService.rollsRoyce(diceRollsDto);

            // THEN
            inOrder.verify(diceMapper).mapToDiceRolls(diceRollsDto);
            inOrder.verify(diceMapper).createSimulationId(diceRolls);
            inOrder.verify(repository).findById(id);
            inOrder.verify(simulationStatistics).updateStatistics(rolls);
            inOrder.verify(repository).saveAndFlush(simulationStatistics);
            inOrder.verify(diceMapper).mapToDiceDistributionSimulationDto(simulationStatistics);
            inOrder.verifyNoMoreInteractions();

            // AND
            assertThat(diceDistributionSimulationDto).isSameAs(resultDto);
        }

        @Test
        void statisticsNotFound() {
            // GIVEN by setup and
            when(repository.findById(id)).thenReturn(Optional.empty());
            when(diceMapper.createSimulationStatistics(id)).thenReturn(simulationStatistics);

            // WHEN
            DiceDistributionSimulationDto diceDistributionSimulationDto = rollService.rollsRoyce(diceRollsDto);

            // THEN
            inOrder.verify(diceMapper).mapToDiceRolls(diceRollsDto);
            inOrder.verify(diceMapper).createSimulationId(diceRolls);
            inOrder.verify(repository).findById(id);
            inOrder.verify(diceMapper).createSimulationStatistics(id);
            inOrder.verify(simulationStatistics).updateStatistics(rolls);
            inOrder.verify(repository).saveAndFlush(simulationStatistics);
            inOrder.verify(diceMapper).mapToDiceDistributionSimulationDto(simulationStatistics);
            inOrder.verifyNoMoreInteractions();

            // AND
            assertThat(diceDistributionSimulationDto).isSameAs(resultDto);
        }
    }

    @Test
    void statistics() {
        // GIVEN
        List<SimulationStatistics> simulationStatistics = mock(List.class);
        List<DiceDistributionStatisticsDto> result = mock(List.class);
        when(repository.findAll(Sort.by("id.diceNumber", "id.diceSide"))).thenReturn(simulationStatistics);
        when(diceMapper.mapToDiceDistributionStatistics(simulationStatistics)).thenReturn(result);

        // WHEN
        List<DiceDistributionStatisticsDto> statistics = rollService.statistics();

        // THEN
        assertThat(statistics).isSameAs(result);
    }
}
