package ch.avaloq.demo.controllers;

import ch.avaloq.demo.ErrorHandler;
import ch.avaloq.demo.controllers.dto.DiceDistributionStatisticsDto;
import ch.avaloq.demo.controllers.dto.DiceRollsDto;
import ch.avaloq.demo.controllers.dto.DiceSumDistributionDto;
import ch.avaloq.demo.services.AvaloqValidationException;
import ch.avaloq.demo.services.DiceValidator;
import ch.avaloq.demo.services.RollService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author : istvan.horvath
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class DiceControllerTest {

    @Mock
    RollService rollService;

    @Mock
    DiceValidator diceValidator;

    DiceController controller;

    @InjectMocks
    ErrorHandler errorHandler;

    MockMvc mockMvc;

    @BeforeEach
    void init() {
        controller = new DiceController(rollService, diceValidator);

        mockMvc = MockMvcBuilders.standaloneSetup(controller, errorHandler).build();
    }

    @Nested
    class DiceRollTests {

        @Test
        void shouldRollDice() {
            DiceRollsDto diceRollsDto = mock(DiceRollsDto.class);

            controller.rollDices(diceRollsDto);

            InOrder inOrder = Mockito.inOrder(diceRollsDto, diceValidator, rollService);

            inOrder.verify(diceValidator).validate(diceRollsDto);
            inOrder.verify(rollService).rollsRoyce(diceRollsDto);

            inOrder.verifyNoMoreInteractions();
        }

        @Test
        void validationFails() throws Exception {
            List<String> errors = List.of("Error message");
            doThrow(new AvaloqValidationException(errors)).when(diceValidator).validate(Mockito.any());

            mockMvc.perform(get("/dice/"))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("errors", is(List.of("Error message"))));

            InOrder inOrder = Mockito.inOrder(diceValidator, rollService);

            inOrder.verify(diceValidator).validate(Mockito.any());

            inOrder.verifyNoMoreInteractions();
        }
    }

    @Nested
    class DiceStatisticsTests {

        @Test
        void test() throws Exception {
            List<DiceDistributionStatisticsDto> diceDistributionStatisticDtos = new ArrayList<>();
            DiceDistributionStatisticsDto distributionStatistics = DiceDistributionStatisticsDto.builder()
                    .diceNumber(3).diceSide(7).totalRolls(123).totalSimulations(5)
                    .diceSumDistributions(List.of(new DiceSumDistributionDto(18L, new BigDecimal("3.665").setScale(2, RoundingMode.HALF_UP)),
                            new DiceSumDistributionDto(3L, new BigDecimal("5.934").setScale(2, RoundingMode.HALF_UP))))
                    .build();
            diceDistributionStatisticDtos.add(distributionStatistics);

            when(rollService.statistics()).thenReturn(diceDistributionStatisticDtos);

            mockMvc.perform(get("/dice/statistics"))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(1)))
                    .andExpect(jsonPath("$.[0].diceNumber", is(3)))
                    .andExpect(jsonPath("$.[0].diceSide", is(7)))
                    .andExpect(jsonPath("$.[0].totalSimulations", is(5)))
                    .andExpect(jsonPath("$.[0].totalRolls", is(123)))
                    .andExpect(jsonPath("$.[0].diceSumDistributions", hasSize(2)))
                    .andExpect(jsonPath("$.[0].diceSumDistributions.[0].sum", is(18)))
                    .andExpect(jsonPath("$.[0].diceSumDistributions.[0].distribution", is(3.67)))
                    .andExpect(jsonPath("$.[0].diceSumDistributions.[1].sum", is(3)))
                    .andExpect(jsonPath("$.[0].diceSumDistributions.[1].distribution", is(5.93)));

            verify(rollService).statistics();
        }
    }
}
