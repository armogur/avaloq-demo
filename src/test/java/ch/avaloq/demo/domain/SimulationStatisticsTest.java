package ch.avaloq.demo.domain;

import ch.avaloq.demo.services.Rolls;
import org.junit.jupiter.api.Test;
import org.reflections.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * @author : istvan.horvath
 */
public class SimulationStatisticsTest {

    @Test
    void noSetters() {
        Set<Method> set = ReflectionUtils.getMethods(SimulationStatistics.class, ReflectionUtils.withPrefix("set"));

        assertThat(set).isEmpty();
    }

    @Test
    void sumToRollsIsImmutable() {
        assertThatThrownBy(() -> new SimulationStatistics().getSumToRollTimes().put(1L, 2L))
                .isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void idConstructorTest() {
        SimulationStatistics.SimulationId id = new SimulationStatistics.SimulationId(3, 6);

        SimulationStatistics simulationStatistics = new SimulationStatistics(id);

        assertThat(simulationStatistics.getId().getDiceNumber()).isEqualTo(3);
        assertThat(simulationStatistics.getId().getDiceSide()).isEqualTo(6);

        assertThat(simulationStatistics.getTotalSimulations()).isEqualTo(0L);
        assertThat(simulationStatistics.getTotalRolls()).isEqualTo(0L);
        assertThat(simulationStatistics.getSumToRollTimes()).isEmpty();
    }

    @Test
    void updateStatistics() {
        SimulationStatistics.SimulationId id = new SimulationStatistics.SimulationId(3, 6);

        SimulationStatistics simulationStatistics = new SimulationStatistics(id);

        // First update
        simulationStatistics.updateStatistics(new Rolls(300, Map.of(3L, 4L, 4L, 3L, 5L, 11L)));

        assertThat(simulationStatistics.getTotalSimulations()).isEqualTo(1);
        assertThat(simulationStatistics.getTotalRolls()).isEqualTo(300);
        assertThat(simulationStatistics.getSumToRollTimes()).isEqualTo(Map.of(3L, 4L, 4L, 3L, 5L, 11L));

        // Second update
        simulationStatistics.updateStatistics(new Rolls(17, Map.of(3L, 1L, 5L, 3L, 7L, 17L)));

        assertThat(simulationStatistics.getTotalSimulations()).isEqualTo(2);
        assertThat(simulationStatistics.getTotalRolls()).isEqualTo(317);
        assertThat(simulationStatistics.getSumToRollTimes()).isEqualTo(Map.of(3L, 5L, 4L, 3L, 5L, 14L, 7L, 17L));
    }
}
