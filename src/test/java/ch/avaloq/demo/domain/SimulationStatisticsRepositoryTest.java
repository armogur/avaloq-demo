package ch.avaloq.demo.domain;

import ch.avaloq.demo.services.RollService;
import ch.avaloq.demo.services.Rolls;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author : istvan.horvath
 */
@DataJpaTest
@TestPropertySource(properties = {
        "spring.datasource.driver-class-name=org.h2.Driver",
        "spring.datasource.url=jdbc:h2:mem:testdb;MODE=PostgreSQL;DB_CLOSE_DELAY=-1",
        "spring.jpa.database-platform=org.hibernate.dialect.PostgreSQLDialect"
})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SimulationStatisticsRepositoryTest {

    @Autowired
    private SimulationStatisticsRepository repository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void saveAndUpdateTest() {
        SimulationStatistics.SimulationId id = new SimulationStatistics.SimulationId(3, 6);

        SimulationStatistics simulationStatistics = new SimulationStatistics(id);

        // First update
        simulationStatistics.updateStatistics(new Rolls(300, Map.of(3L, 4L, 4L, 3L, 5L, 11L)));

        Optional<SimulationStatistics> byId = repository.findById(id);
        assertThat(byId).isNotPresent();

        repository.save(simulationStatistics);

        testEntityManager.flush();
        testEntityManager.clear();

        assertThat(repository.findById(id)).isPresent();
    }

    @Test
    void findTest() {
        assertThat(repository.findAll(RollService.SORT)).isEmpty();

        repository.save(new SimulationStatistics(new SimulationStatistics.SimulationId(3, 6)));
        repository.save(new SimulationStatistics(new SimulationStatistics.SimulationId(2, 6)));
        repository.save(new SimulationStatistics(new SimulationStatistics.SimulationId(3, 7)));
        repository.save(new SimulationStatistics(new SimulationStatistics.SimulationId(3, 5)));
        repository.save(new SimulationStatistics(new SimulationStatistics.SimulationId(2, 4)));

        List<SimulationStatistics> all = repository.findAll(RollService.SORT);

        assertThat(all).hasSize(5);
        assertThat(all.stream().map(SimulationStatistics::getId)).containsExactly(
                new SimulationStatistics.SimulationId(2, 4),
                new SimulationStatistics.SimulationId(2, 6),
                new SimulationStatistics.SimulationId(3, 5),
                new SimulationStatistics.SimulationId(3, 6),
                new SimulationStatistics.SimulationId(3, 7)
        );
    }
}
