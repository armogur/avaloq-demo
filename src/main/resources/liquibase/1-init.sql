create table simulation_statistics (
    dice_number       integer not null,
    dice_side         integer not null,
    total_rolls       bigint  not null,
    total_simulations bigint  not null,
    primary key (dice_number, dice_side)
);

create table roll_sum_to_roll_times (
    simulation_statistics_dice_number integer not null,
    simulation_statistics_dice_side   integer not null,
    sum_to_roll_times                 bigint,
    roll_sum                          bigint  not null,
    primary key (simulation_statistics_dice_number, simulation_statistics_dice_side, roll_sum),
    constraint fk_roll_sum_to_simulation_statistics
    foreign key (simulation_statistics_dice_number, simulation_statistics_dice_side)
        references simulation_statistics
);