package ch.avaloq.demo.controllers;

import ch.avaloq.demo.controllers.dto.DiceDistributionSimulationDto;
import ch.avaloq.demo.controllers.dto.DiceDistributionStatisticsDto;
import ch.avaloq.demo.controllers.dto.DiceRollsDto;
import ch.avaloq.demo.services.DiceValidator;
import ch.avaloq.demo.services.RollService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author : istvan.horvath
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("dice")
public class DiceController {

    private final RollService rollService;
    private final DiceValidator diceValidator;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public DiceDistributionSimulationDto rollDices(DiceRollsDto diceRollsDto) {
        diceValidator.validate(diceRollsDto);
        return rollService.rollsRoyce(diceRollsDto);
    }

    @GetMapping(path = "statistics")
    public List<DiceDistributionStatisticsDto> statistics() {
        return rollService.statistics();
    }
}
