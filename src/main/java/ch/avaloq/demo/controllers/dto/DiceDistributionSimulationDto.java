package ch.avaloq.demo.controllers.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : istvan.horvath
 */
@Builder
@Value
public class DiceDistributionSimulationDto {
    int diceNumber;
    int diceSide;

    long totalSimulations;
    long totalRolls;

    @Builder.Default
    List<DiceSumToRollDto> sumToRoll = new ArrayList<>();
}
