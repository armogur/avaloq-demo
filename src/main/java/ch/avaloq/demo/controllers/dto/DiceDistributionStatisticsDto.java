package ch.avaloq.demo.controllers.dto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

/**
 * @author : istvan.horvath
 */
@Builder
@Value
public class DiceDistributionStatisticsDto {
    int diceNumber;
    int diceSide;
    long totalSimulations;
    long totalRolls;
    List<DiceSumDistributionDto> diceSumDistributions;
}
