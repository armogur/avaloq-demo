package ch.avaloq.demo.controllers.dto;

import lombok.*;

/**
 * @author : istvan.horvath
 */
@Value
public class DiceSumToRollDto {
    Long sum;
    Long roll;
}
