package ch.avaloq.demo.controllers.dto;

import lombok.Value;

/**
 * @author : istvan.horvath
 */
@Value
public class DiceRollsDto {
    Integer dices;
    Integer sides;
    Integer rolls;
}
