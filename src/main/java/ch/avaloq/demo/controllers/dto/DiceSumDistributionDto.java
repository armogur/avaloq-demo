package ch.avaloq.demo.controllers.dto;

import lombok.Value;

import java.math.BigDecimal;

/**
 * @author : istvan.horvath
 */
@Value
public class DiceSumDistributionDto {
    Long sum;
    BigDecimal distribution;
}
