package ch.avaloq.demo;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.ConstructorBinding;

import java.util.Optional;

/**
 * @author : istvan.horvath
 */
@ConfigurationProperties(prefix = "application")
@Builder
@Getter
@FieldDefaults(makeFinal=true, level= AccessLevel.PRIVATE)
public class ApplicationConfiguration {

    @ConstructorBinding
    public ApplicationConfiguration(Integer minDices, Integer minSides, Integer minRolls,
                                    Integer defaultDices, Integer defaultSides, Integer defaultRolls) {
        this.minDices = Optional.ofNullable(minDices).orElse(1);
        this.minSides = Optional.ofNullable(minSides).orElse(4);
        this.minRolls = Optional.ofNullable(minRolls).orElse(1);
        this.defaultDices = Optional.ofNullable(defaultDices).orElse(3);
        this.defaultSides = Optional.ofNullable(defaultSides).orElse(6);
        this.defaultRolls = Optional.ofNullable(defaultRolls).orElse(100);
    }

    int minDices;
    int minSides;
    int minRolls;
    int defaultDices;
    int defaultSides;
    int defaultRolls;
}
