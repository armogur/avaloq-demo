package ch.avaloq.demo.domain;

import ch.avaloq.demo.services.Rolls;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author : istvan.horvath
 */
@Entity
@Table(name = "simulation_statistics")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Builder
public class SimulationStatistics {

    @EmbeddedId
    private SimulationId id;

    private long totalSimulations;
    private long totalRolls;

    @CollectionTable(name = "roll_sum_to_roll_times")
    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = "roll_sum")
    private final Map<Long, Long> sumToRollTimes = new HashMap<>();

    @Embeddable
    @Getter
    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    @AllArgsConstructor
    @EqualsAndHashCode
    public static final class SimulationId implements Serializable {
        private int diceNumber;
        private int diceSide;
    }

    public SimulationStatistics(SimulationId id) {
        this.id = id;
        this.totalSimulations = 0;
        this.totalRolls = 0;
    }

    public Map<Long, Long> getSumToRollTimes() {
        return Map.copyOf(sumToRollTimes);
    }

    public void updateStatistics(Rolls rolls) {
        this.totalSimulations++;
        this.totalRolls += rolls.getRolls();

        for (Long rollSum : rolls.getSumToRollTimes().keySet()) {
            Long rollSumCount = rolls.getSumToRollTimes().get(rollSum);
            this.sumToRollTimes.compute(rollSum, (k, v) -> v == null ? rollSumCount : v + rollSumCount);
        }
    }
}
