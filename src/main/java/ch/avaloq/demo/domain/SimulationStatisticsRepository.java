package ch.avaloq.demo.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author : istvan.horvath
 */
@Repository
public interface SimulationStatisticsRepository extends
        JpaRepository<SimulationStatistics, SimulationStatistics.SimulationId> {

}
