package ch.avaloq.demo;

import ch.avaloq.demo.controllers.dto.DiceRollsDto;
import ch.avaloq.demo.domain.SimulationStatistics;
import org.postgresql.util.PGobject;
import org.springframework.aot.hint.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author : istvan.horvath
 */
public class AvaloqDemoRuntimeHints implements RuntimeHintsRegistrar {

    @Override
    public void registerHints(RuntimeHints hints, ClassLoader classLoader) {
        try {
            List<Class<?>> classes = List.of(DiceRollsDto.class, PGobject.class, ArrayList.class);
            for (Class<?> clazz : classes) {
                for (Constructor<?> constructor : clazz.getConstructors()) {
                    hints.reflection().registerConstructor(constructor, ExecutableMode.INVOKE);
                }
            }
            List<Method> methods = new ArrayList<>(List.of(
                    SimulationStatistics.SimulationId.class.getMethod("equals", Object.class),
                    SimulationStatistics.SimulationId.class.getMethod("hashCode")));
            for (Method method : methods) {
                hints.reflection().registerMethod(method, ExecutableMode.INVOKE);
            }
            liquibaseHints(hints);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private void liquibaseHints(RuntimeHints hints) {
        Consumer<ResourcePatternHints.Builder> builderConsumer = builder -> builder.includes(
                "liquibase/db.changelog-master.xml", "liquibase/1-init.sql");
        hints.resources().registerPattern(builderConsumer);

        Consumer<TypeHint.Builder> typeHint = builder -> builder.withMembers(
                MemberCategory.INVOKE_PUBLIC_CONSTRUCTORS, MemberCategory.PUBLIC_FIELDS,
                MemberCategory.INVOKE_PUBLIC_METHODS);
        hints.reflection().registerTypes(
                TypeReference.listOf(
                        liquibase.AbstractExtensibleObject.class,
                        liquibase.change.AbstractChange.class,
                        liquibase.change.AbstractSQLChange.class,
                        liquibase.change.ChangeFactory.class,
                        liquibase.change.ColumnConfig.class,
                        liquibase.change.ConstraintsConfig.class,
                        liquibase.change.core.AbstractModifyDataChange.class,
                        liquibase.change.core.AddAutoIncrementChange.class,
                        liquibase.change.core.AddColumnChange.class,
                        liquibase.change.core.AddDefaultValueChange.class,
                        liquibase.change.core.AddForeignKeyConstraintChange.class,
                        liquibase.change.core.AddLookupTableChange.class,
                        liquibase.change.core.AddNotNullConstraintChange.class,
                        liquibase.change.core.AddPrimaryKeyChange.class,
                        liquibase.change.core.AddUniqueConstraintChange.class,
                        liquibase.change.core.AlterSequenceChange.class,
                        liquibase.change.core.CreateIndexChange.class,
                        liquibase.change.core.CreateProcedureChange.class,
                        liquibase.change.core.CreateSequenceChange.class,
                        liquibase.change.core.CreateTableChange.class,
                        liquibase.change.core.CreateViewChange.class,
                        liquibase.change.core.DeleteDataChange.class,
                        liquibase.change.core.DropAllForeignKeyConstraintsChange.class,
                        liquibase.change.core.DropColumnChange.class,
                        liquibase.change.core.DropDefaultValueChange.class,
                        liquibase.change.core.DropForeignKeyConstraintChange.class,
                        liquibase.change.core.DropIndexChange.class,
                        liquibase.change.core.DropNotNullConstraintChange.class,
                        liquibase.change.core.DropPrimaryKeyChange.class,
                        liquibase.change.core.DropProcedureChange.class,
                        liquibase.change.core.DropSequenceChange.class,
                        liquibase.change.core.DropTableChange.class,
                        liquibase.change.core.DropUniqueConstraintChange.class,
                        liquibase.change.core.DropViewChange.class,
                        liquibase.change.core.EmptyChange.class,
                        liquibase.change.core.ExecuteShellCommandChange.class,
                        liquibase.change.core.InsertDataChange.class,
                        liquibase.change.core.LoadDataChange.class,
                        liquibase.change.core.LoadUpdateDataChange.class,
                        liquibase.change.core.MergeColumnChange.class,
                        liquibase.change.core.ModifyDataTypeChange.class,
                        liquibase.change.core.OutputChange.class,
                        liquibase.change.core.RawSQLChange.class,
                        liquibase.change.core.RenameColumnChange.class,
                        liquibase.change.core.RenameSequenceChange.class,
                        liquibase.change.core.RenameTableChange.class,
                        liquibase.change.core.RenameViewChange.class,
                        liquibase.change.core.SQLFileChange.class,
                        liquibase.change.core.SetColumnRemarksChange.class,
                        liquibase.change.core.SetTableRemarksChange.class,
                        liquibase.change.core.StopChange.class,
                        liquibase.change.core.TagDatabaseChange.class,
                        liquibase.change.core.UpdateDataChange.class,
                        liquibase.change.custom.CustomChangeWrapper.class,
                        liquibase.changelog.StandardChangeLogHistoryService.class,
                        liquibase.GlobalConfiguration.class,
                        liquibase.hub.HubConfiguration.class,
                        liquibase.database.core.H2Database.class,
                        liquibase.database.core.MariaDBDatabase.class,
                        liquibase.database.core.MSSQLDatabase.class,
                        liquibase.database.core.MySQLDatabase.class,
                        liquibase.database.core.OracleDatabase.class,
                        liquibase.database.core.PostgresDatabase.class,
                        liquibase.database.jvm.JdbcConnection.class,
                        liquibase.datatype.core.BigIntType.class,
                        liquibase.datatype.core.BlobType.class,
                        liquibase.datatype.core.BooleanType.class,
                        liquibase.datatype.core.CharType.class,
                        liquibase.datatype.core.ClobType.class,
                        liquibase.datatype.core.CurrencyType.class,
                        liquibase.datatype.core.DatabaseFunctionType.class,
                        liquibase.datatype.core.DateTimeType.class,
                        liquibase.datatype.core.DateType.class,
                        liquibase.datatype.core.DecimalType.class,
                        liquibase.datatype.core.DoubleType.class,
                        liquibase.datatype.core.FloatType.class,
                        liquibase.datatype.core.IntType.class,
                        liquibase.datatype.core.MediumIntType.class,
                        liquibase.datatype.core.NCharType.class,
                        liquibase.datatype.core.NumberType.class,
                        liquibase.datatype.core.NVarcharType.class,
                        liquibase.datatype.core.SmallIntType.class,
                        liquibase.datatype.core.TimestampType.class,
                        liquibase.datatype.core.TimeType.class,
                        liquibase.datatype.core.TinyIntType.class,
                        liquibase.datatype.core.UnknownType.class,
                        liquibase.datatype.core.UUIDType.class,
                        liquibase.datatype.core.VarcharType.class,
                        liquibase.datatype.core.XMLType.class,
                        liquibase.executor.ExecutorService.class,
                        liquibase.executor.jvm.JdbcExecutor.class,
                        liquibase.hub.HubServiceFactory.class,
                        liquibase.license.LicenseServiceFactory.class,
                        liquibase.lockservice.StandardLockService.class,
                        liquibase.logging.core.LogServiceFactory.class,
                        liquibase.parser.ChangeLogParserConfiguration.class,
                        liquibase.plugin.AbstractPlugin.class,
                        liquibase.serializer.AbstractLiquibaseSerializable.class,
                        liquibase.sql.visitor.AppendSqlVisitor.class,
                        liquibase.sql.visitor.PrependSqlVisitor.class,
                        liquibase.sql.visitor.RegExpReplaceSqlVisitor.class,
                        liquibase.sql.visitor.ReplaceSqlVisitor.class,
                        liquibase.changelog.RanChangeSet.class,
                        com.zaxxer.hikari.HikariConfig.class,
                        java.util.concurrent.CopyOnWriteArrayList.class,
                        java.sql.Statement[].class,
                        com.zaxxer.hikari.HikariDataSource.class,
                        liquibase.configuration.LiquibaseConfiguration.class,
                        com.zaxxer.hikari.util.ConcurrentBag.IConcurrentBagEntry[].class,
                        liquibase.change.core.LoadDataColumnConfig.class)
                , typeHint);
    }
}
