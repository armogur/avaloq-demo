package ch.avaloq.demo.services;

import lombok.Value;

import java.util.Map;

@Value
public class Rolls {
    int rolls;
    Map<Long, Long> sumToRollTimes;
}
