package ch.avaloq.demo.services;

import ch.avaloq.demo.ApplicationConfiguration;
import ch.avaloq.demo.controllers.dto.*;
import ch.avaloq.demo.domain.SimulationStatistics;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class DiceMapper {

    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100);
    private final ApplicationConfiguration applicationConfiguration;

    DiceRolls mapToDiceRolls(DiceRollsDto diceRollsDto) {
        int dices = Objects.requireNonNullElse(diceRollsDto.getDices(), applicationConfiguration.getDefaultDices());
        int sides = Objects.requireNonNullElse(diceRollsDto.getSides(), applicationConfiguration.getDefaultSides());
        int rolls = Objects.requireNonNullElse(diceRollsDto.getRolls(), applicationConfiguration.getDefaultRolls());
        return new DiceRolls(dices, sides, rolls);
    }

    SimulationStatistics.SimulationId createSimulationId(DiceRolls diceRolls) {
        return new SimulationStatistics.SimulationId(diceRolls.getDices(), diceRolls.getSides());
    }

    SimulationStatistics createSimulationStatistics(SimulationStatistics.SimulationId id) {
        return new SimulationStatistics(id);
    }

    DiceDistributionSimulationDto mapToDiceDistributionSimulationDto(SimulationStatistics simulationStatistics) {
        return DiceDistributionSimulationDto.builder()
                .diceNumber(simulationStatistics.getId().getDiceNumber())
                .diceSide(simulationStatistics.getId().getDiceSide())
                .totalSimulations(simulationStatistics.getTotalSimulations())
                .totalRolls(simulationStatistics.getTotalRolls())
                .sumToRoll(simulationStatistics.getSumToRollTimes().entrySet()
                        .stream().map(a -> new DiceSumToRollDto(a.getKey(), a.getValue())).toList())
                .build();
    }

    List<DiceDistributionStatisticsDto> mapToDiceDistributionStatistics(List<SimulationStatistics> simulationStatistics) {
        return simulationStatistics.stream().map(this::mapToDiceDistributionStatistics).toList();
    }

    DiceDistributionStatisticsDto mapToDiceDistributionStatistics(SimulationStatistics simulationStatistics) {
        List<DiceSumDistributionDto> diceSumDistributionDtoList = mapDiceSumDistributions(
                simulationStatistics.getSumToRollTimes(), simulationStatistics.getTotalRolls());
        return DiceDistributionStatisticsDto.builder()
                .diceNumber(simulationStatistics.getId().getDiceNumber())
                .diceSide(simulationStatistics.getId().getDiceSide())
                .totalSimulations(simulationStatistics.getTotalSimulations())
                .totalRolls(simulationStatistics.getTotalRolls())
                .diceSumDistributions(diceSumDistributionDtoList)
                .build();
    }

    List<DiceSumDistributionDto> mapDiceSumDistributions(Map<Long, Long> sumToRollTimes, long totalRolls) {
        return sumToRollTimes.entrySet().stream().map(a ->
                        new DiceSumDistributionDto(a.getKey(), HUNDRED
                                .multiply(BigDecimal.valueOf(a.getValue()))
                                .divide(BigDecimal.valueOf(totalRolls), 2, RoundingMode.HALF_UP)))
                .sorted(Comparator.comparingLong(DiceSumDistributionDto::getSum)).toList();
    }
}
