package ch.avaloq.demo.services;

import ch.avaloq.demo.ApplicationConfiguration;
import ch.avaloq.demo.controllers.dto.DiceRollsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : istvan.horvath
 */
@RequiredArgsConstructor
@Service
public class DiceValidator {

    private final ApplicationConfiguration applicationConfiguration;

    public void validate(DiceRollsDto diceRollsDto) {
        List<String> errors = new ArrayList<>();
        if (diceRollsDto.getDices() != null && diceRollsDto.getDices() < applicationConfiguration.getMinDices()) {
            errors.add(String.format("The number of dices should be at least %d, now: %d",
                    applicationConfiguration.getMinDices(), diceRollsDto.getDices()));
        }
        if (diceRollsDto.getSides() != null && diceRollsDto.getSides() < applicationConfiguration.getMinSides()) {
            errors.add(String.format("The number of sides should be at least %d, now: %d",
                    applicationConfiguration.getMinSides(), diceRollsDto.getSides()));
        }
        if (diceRollsDto.getRolls() != null && diceRollsDto.getRolls() < applicationConfiguration.getMinRolls()) {
            errors.add(String.format("The number of rolls should be at least %d, now: %d",
                    applicationConfiguration.getMinRolls(), diceRollsDto.getRolls()));
        }
        if (!errors.isEmpty()) {
            throw new AvaloqValidationException(errors);
        }
    }
}
