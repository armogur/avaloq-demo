package ch.avaloq.demo.services;

import ch.avaloq.demo.controllers.dto.DiceDistributionSimulationDto;
import ch.avaloq.demo.controllers.dto.DiceDistributionStatisticsDto;
import ch.avaloq.demo.controllers.dto.DiceRollsDto;
import ch.avaloq.demo.domain.SimulationStatistics;
import ch.avaloq.demo.domain.SimulationStatisticsRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : istvan.horvath
 */
@Service
@RequiredArgsConstructor
public class RollService {
    public static final Sort SORT = Sort.by("id.diceNumber", "id.diceSide");

    private final SimulationStatisticsRepository simulationStatisticsRepository;

    private final DiceMapper diceMapper;

    @Transactional
    public DiceDistributionSimulationDto rollsRoyce(DiceRollsDto diceRollsDto) {
        DiceRolls diceRolls = diceMapper.mapToDiceRolls(diceRollsDto);
        SimulationStatistics.SimulationId id = diceMapper.createSimulationId(diceRolls);
        SimulationStatistics simulationStatistics = simulationStatisticsRepository.findById(id)
                .orElse(diceMapper.createSimulationStatistics(id));
        simulationStatistics.updateStatistics(diceRolls.simulateRolls());

        simulationStatisticsRepository.saveAndFlush(simulationStatistics);

        return diceMapper.mapToDiceDistributionSimulationDto(simulationStatistics);
    }

    public List<DiceDistributionStatisticsDto> statistics() {
        List<SimulationStatistics> simulationStatistics = simulationStatisticsRepository.findAll(SORT);
        return diceMapper.mapToDiceDistributionStatistics(simulationStatistics);
    }
}
