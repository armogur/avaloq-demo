package ch.avaloq.demo.services;

import lombok.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author : istvan.horvath
 */
@Value
public class DiceRolls {
    int dices;
    int sides;
    int rolls;

    Rolls simulateRolls() {
        Map<Long, Long> sumToRollTimes = new HashMap<>();
        for (int i = 0; i < rolls; i++) {
            long sum = new Random().longs(dices, 1, sides + 1).sum();
            sumToRollTimes.compute(sum, (k, v) -> v == null ? 1 : v + 1);
        }
        return new Rolls(rolls, sumToRollTimes);
    }
}
