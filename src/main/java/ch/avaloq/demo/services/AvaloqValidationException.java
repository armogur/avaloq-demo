package ch.avaloq.demo.services;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * test.
 *
 * @author : istvan.horvath
 */
@AllArgsConstructor
@Getter
public class AvaloqValidationException extends RuntimeException {
    private final List<String> errors;
}
