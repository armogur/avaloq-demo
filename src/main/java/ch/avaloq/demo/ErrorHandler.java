package ch.avaloq.demo;

import ch.avaloq.demo.services.AvaloqValidationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Map;

/**
 * @author : istvan.horvath
 */
@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatusCode status,
                                                                  WebRequest request) {
        return new ResponseEntity<>(ex.getBindingResult().getFieldErrors().stream().map(Violation::new).toList(),
                null, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AvaloqValidationException.class)
    protected ResponseEntity<Object> handleAvaloqException(AvaloqValidationException ex) {
        return new ResponseEntity<>(
                Map.of( "errors", ex.getErrors()), null, HttpStatus.BAD_REQUEST);
    }

    record Violation(String field, String message) {
        Violation(FieldError fieldError) {
            this(fieldError.getField(), fieldError.getDefaultMessage());
        }
    }
}
